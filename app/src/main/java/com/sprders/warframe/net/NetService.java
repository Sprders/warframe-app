package com.sprders.warframe.net;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * 获取
 *
 * @author sprders
 */
public interface NetService {
    /**
     * 获取
     *
     * @param platform pc,xb1,ps4
     * @param type     数据分类的类型
     * @return 结果
     */
    @GET("{platform}/{type}")
    Call<ResponseBody> getDataFromServer(@Path("platform") String platform,
                                         @Path("type") String type);
}
