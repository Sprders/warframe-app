package com.sprders.warframe.thread;

import com.sprders.warframe.App;
import com.sprders.warframe.R;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

public final class CopyDbThread extends Thread{
    @Override
    public void run() {
        int[] resIds = {R.raw.wf_alert_invasion, R.raw.wf_dict};
        String[] filenames = {"wf_alert_invasion.db", "wf_dict.db"};
        for (int i = 0; i < resIds.length; i++) {
            syncDb(resIds[i], filenames[i]);
        }
    }

    public void syncDb(int resourceId, String filename) {
        InputStream inputStream = App.getContext().getResources().openRawResource(resourceId);
        File filesDir = App.getContext().getFilesDir();
        File des = new File(filesDir, filename);
        FileOutputStream outputStream = null;
        try {
            if (des.exists()) {
                return;
            }
//            des.createNewFile();
            outputStream = new FileOutputStream(des);
            byte bt[] = new byte[1024];
            int c;
            while ((c = inputStream.read(bt)) > 0) {
                outputStream.write(bt, 0, c);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (outputStream != null) {
                    outputStream.close();
                }
                inputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
