package com.sprders.warframe.thread;

import android.os.SystemClock;
import android.util.Log;

import com.sprders.warframe.utils.PerferencesUtils;
import com.sprders.warframe.view.base.BaseFragment;

public class UpdateThread extends Thread {

    private BaseFragment mBaseFragment;
    private static final String TAG = "UpdateThread";

    private boolean flag = true;

    public UpdateThread() {
    }

    public UpdateThread(BaseFragment baseFragment) {
        mBaseFragment = baseFragment;
    }

    @Override
    public void run() {
        Log.e(TAG, this.toString() + "  线程已启动");
        while (flag) {
            final long sleepTime = 185000;
            long updateTime = PerferencesUtils.getUpdateTime();
            if (System.currentTimeMillis() - updateTime > sleepTime && mBaseFragment != null) {
                Log.e(TAG, this.toString() + "  开始更新");
                mBaseFragment.refreshView();
            } else {
                Log.e(TAG, this.toString() + "  开始休眠 31s ");
                SystemClock.sleep(31000);
            }
        }
    }

    public void stopThread() {
        Log.e(TAG, "stopThread: ");
        flag = false;
    }

    public void setBaseFragment(BaseFragment baseFragment) {
        mBaseFragment = baseFragment;
    }

    public BaseFragment getBaseFragment() {
        return mBaseFragment;
    }
}
