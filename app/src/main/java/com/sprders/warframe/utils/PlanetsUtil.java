package com.sprders.warframe.utils;

import java.util.Objects;

public final class PlanetsUtil {

    public static String coverEn2Zh(String en) {
        String[] zhs = {
                "水星", "金星", "地球", "月球", "火星", "火卫一",
                "谷神星", "木星", "欧罗巴", "土星", "天王星", "海王星",
                "冥王星", "赛德娜", "赤毒要塞", "阋神星", "虚空", "遗迹"
        };
        String[] ens = {
                "Mercury", "Venus", "Earth", "Lua", "Mars", "Phobos",
                "Ceres", "Jupiter", "Europa", "Saturn", "Uranus", "Neptune",
                "Pluto", "Sedna", "Kuva Fortress", "Eris", "Void", "Derelict"
        };
        String result = en;
        for (int i = 0, length = ens.length; i < length; i++) {
            if (Objects.equals(ens[i], en)) {
                result = zhs[i];
                break;
            }
        }
        return result;
    }
}
