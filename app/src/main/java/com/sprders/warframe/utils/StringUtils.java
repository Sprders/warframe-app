package com.sprders.warframe.utils;


public final class StringUtils {

    private StringUtils() {
    }

    /**
     * 字符串是否含有数字
     */
    public static boolean containDigital(String string) {
        if (isEmpty(string)) {
            return false;
        }
        for (int i = 0, length = string.length(); i < length; i++) {
            char c = string.charAt(i);
            if (Character.isDigit(c)) {
                return true;
            }
        }
        return false;
    }

    /**
     * 验证字符串全由字母构成
     */
    public static boolean isLetters(String string) {
        return !isEmpty(string) && string.matches("[a-zA-Z]+");
    }

    /**
     * 验证字符串是否是小数
     */
    public static boolean isDecimal(String string) {
        return !isEmpty(string) && string.matches("\\d+\\.\\d+");
    }

    /**
     * 验证字符串是整数
     */
    public static boolean isInteger(String string) {
        return !isEmpty(string) && string.matches("\\d+");
    }

    /**
     * 字符串是否为空（null 或 ""）
     */
    public static boolean isEmpty(String string) {
        return string == null || string.length() <= 0;
    }

}