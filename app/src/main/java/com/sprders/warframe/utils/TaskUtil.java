package com.sprders.warframe.utils;

import java.util.Objects;

public final class TaskUtil {

    public static String coverEn2Zh(String en) {
        String[] zhs = {
                "竞技场", "指数之场", "Rathuum", "刺杀", "强袭",
                "捕获", "防御", "挖掘", "歼灭", "劫持",
                "Infested资源回收", "拦截", "接合点", "移动防御", "追击",
                "重整态势", "救援", "突袭(Archwing)", "破坏", "圣殿突袭",
                "间谍", "叛逃", "生存", "试炼"
        };
        String[] ens = {
                "Arena", "The Index", "Rathuum", "Assassination", "Assault",
                "Capture", "Defense", "Excavation", "Extermination", "Hijack",
                "Infested Salvage", "Interception", "Junction", "Mobile Defense", "Pursuit",
                "Recovery", "Rescue", "Rush (Archwing)", "Sabotage", "Sanctuary Onslaught",
                "Spy", "Defection", "Survival", "Trial"
        };
        String result = en;
        for (int i = 0, length = ens.length; i < length; i++) {
            if (Objects.equals(ens[i], en)) {
                result = zhs[i];
                break;
            }
        }
        return result;
    }
}
