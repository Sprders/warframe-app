package com.sprders.warframe.utils;

import com.sprders.warframe.db.AlertsInvasionsHelper;
import com.sprders.warframe.model.common.CountedItemBean;

import java.util.List;

public final class ModelUtils {

    private ModelUtils() {
    }

    public static String translateNode2Zh(String node) {
        int start = node.indexOf('(');
        String planet = node.substring(start + 1, node.length() - 1);
        return node.replace(planet, PlanetsUtil.coverEn2Zh(planet));
    }

    public static String translateCountedItem2Zh(List<CountedItemBean> list) {
        StringBuilder stringBuilder = new StringBuilder("");
        for (CountedItemBean countedItem : list) {
            stringBuilder.append("+");
            if (countedItem.count != 1) {
                stringBuilder.append(countedItem.count).append(" ");
            }
            stringBuilder.append(AlertsInvasionsHelper.getZh(countedItem.type));
        }
        return stringBuilder.toString();
    }

    private static final String TAG = "ModelUtils";

    public static String translateItems2Zh(List<String> list) {
        StringBuilder stringBuilder = new StringBuilder("");

        for (String s : list) {
            stringBuilder.append("+");

            if (StringUtils.containDigital(s)) {

//                int i = s.indexOf('(');
//                String item = s.substring(i + 1, s.length() - 1);
//                Log.e(TAG, "translateItems2Zh: " + item);
                String[] split = s.split(" ");
                for (String s1 : split) {
                    if (StringUtils.isInteger(s1.trim())) {
                        stringBuilder.append(s1.trim()).append(" ");
                    }
                    if (StringUtils.isLetters(s1.trim())) {
                        stringBuilder.append(AlertsInvasionsHelper.getZh(s1.trim()));
                    }
                }
            } else {
                stringBuilder.append(AlertsInvasionsHelper.getZh(s));
            }

        }
        return stringBuilder.toString();
    }
}
