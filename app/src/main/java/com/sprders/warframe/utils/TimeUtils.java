package com.sprders.warframe.utils;

import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

public final class TimeUtils {

    private TimeUtils() {
    }

    public static long parseTime2Long(String time) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.S'Z'");
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone("GMT-0"));
        Date parse = simpleDateFormat.parse(time, new ParsePosition(0));
        if (parse.after(new Date())) {
            return (parse.getTime() - System.currentTimeMillis()) / 1000;
        } else {
            return 0;
        }
    }

    public static String parseLong2Time(long time) {
        if (time <= 0) {
            return "00:00:00";
        }
        long h = time / 3600;
        long m = time % (24 * 3600) % 3600 / 60;
        long s = time % (24 * 3600) % 3600 % 60;
        String result = (h < 10 ? "0" : "") + h;
        result += ":";
        result += ((m < 10 ? "0" : "") + m);
        result += ":";
        result += ((s < 10 ? "0" : "") + s);
        return result;
    }

    private static int parseString2Int(String num) {
        try {
            return Integer.parseInt(num.trim());
        } catch (NumberFormatException e) {
            return 0;
        }
    }
}
