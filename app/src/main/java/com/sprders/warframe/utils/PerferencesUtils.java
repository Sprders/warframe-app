package com.sprders.warframe.utils;

import android.content.Context;

import com.sprders.warframe.App;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public final class PerferencesUtils {
    private static String name = "config";
    private static final String UPDATE_TIME = "UPDATE_TIME";
    private static final String UPDATE_RESULT = "UPDATE_RESULT";

    public static void putInt(String key, int value) {
        App.getContext().getSharedPreferences(name, Context.MODE_PRIVATE)
                .edit()
                .putInt(key, value)
                .apply();
    }

    public static void updateUpdateTime() {

        App.getContext().getSharedPreferences(name, Context.MODE_PRIVATE)
                .edit()
                .putLong(UPDATE_TIME, System.currentTimeMillis())
                .apply();
    }

    public static void setUpdateTime(long time) {
        App.getContext().getSharedPreferences(name, Context.MODE_PRIVATE)
                .edit()
                .putLong(UPDATE_TIME, time)
                .apply();
    }

    public static long getUpdateTime() {
        return App.getContext().getSharedPreferences(name, Context.MODE_PRIVATE)
                .getLong(UPDATE_TIME, 0);
    }

    public static void updateSuccess() {
        App.getContext().getSharedPreferences(name, Context.MODE_PRIVATE)
                .edit()
                .putBoolean(UPDATE_RESULT, true)
                .apply();
    }

    public static void updateFail() {
        App.getContext().getSharedPreferences(name, Context.MODE_PRIVATE)
                .edit()
                .putBoolean(UPDATE_RESULT, false)
                .apply();
    }

    public static boolean getUpdateResult() {
        return App.getContext().getSharedPreferences(name, Context.MODE_PRIVATE)
                .getBoolean(UPDATE_RESULT, false);
    }

}
