package com.sprders.warframe.db;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.sprders.warframe.App;

import java.io.File;

public final class AlertsInvasionsHelper {

    public static String getZh(String en) {
        if (!fileExist()) {
            return en;
        }
        File filesDir = App.getContext().getFilesDir();
        String filename = "wf_alert_invasion.db";
        File des = new File(filesDir, filename);
        SQLiteDatabase sqLiteDatabase = SQLiteDatabase.openDatabase(des.getAbsolutePath(), null, SQLiteDatabase.OPEN_READONLY);
        Cursor query = sqLiteDatabase.query("wf_alert_invasion", new String[]{"zh"}, "en=?", new String[]{en}, null, null, null);
        String result = en;
        if (query.moveToNext()) {
            int zh = query.getColumnIndex("zh");
            result = query.getString(zh);
        }
        query.close();
        sqLiteDatabase.close();
        return result;
    }

    private static boolean fileExist() {
        File filesDir = App.getContext().getFilesDir();
        File des = new File(filesDir, "wf_alert_invasion.db");
        return des.exists();
    }
}
