package com.sprders.warframe.view.base;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.View;

import com.sprders.warframe.App;
import com.sprders.warframe.view.activity.MainActivity;

/**
 * @author sprders
 */
public abstract class BaseFragment extends Fragment {

    protected MainActivity mMainActivity;

    protected Handler mHandler;

    protected final int COUNT_DOWN = 0;
    protected final int SHOW_LOADING = 1;
    protected final int HIDE_LOADING = 2;

    public abstract void refreshView();

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mMainActivity = (MainActivity) context;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        mHandler = new Handler(App.getContext().getMainLooper()) {
            @Override
            public void handleMessage(Message msg) {
                switch (msg.what) {
                    case SHOW_LOADING:
                        mMainActivity.showLoadingDialog();
                        break;
                    case HIDE_LOADING:
                        mMainActivity.dismissLoadingDialog();
                        break;
                    default:
                        BaseFragment.this.handleMessage(msg);
                        break;
                }
            }
        };
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (mHandler != null) {
            mHandler.removeCallbacksAndMessages(null);
        }
        mHandler = null;
    }

    public void handleMessage(Message msg) {
    }
}
