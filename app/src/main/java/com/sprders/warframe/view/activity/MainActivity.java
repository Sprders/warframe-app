package com.sprders.warframe.view.activity;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.util.ArrayMap;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Toast;

import com.qmuiteam.qmui.widget.QMUILoadingView;
import com.qmuiteam.qmui.widget.dialog.QMUITipDialog;
import com.sprders.warframe.Constants;
import com.sprders.warframe.R;
import com.sprders.warframe.thread.UpdateThread;
import com.sprders.warframe.utils.PerferencesUtils;
import com.sprders.warframe.view.base.BaseFragment;
import com.sprders.warframe.view.fragment.AlertsFragment;
import com.sprders.warframe.view.fragment.EmptyFragment;
import com.sprders.warframe.view.fragment.FissuresFragment;
import com.sprders.warframe.view.fragment.HomeFragment;
import com.sprders.warframe.view.fragment.InvasionsFragment;
import com.sprders.warframe.view.fragment.SortieFragment;

import java.util.Objects;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, SwipeRefreshLayout.OnRefreshListener {
    private static final String TAG = "MainActivity";
    private DrawerLayout mDrawerLayout;
    private Toolbar mToolbar;
    private ArrayMap<String, BaseFragment> mFragmentMap;
    private BaseFragment mCurrentFragment;
    private Handler mHandler;
    private UpdateThread mUpdateThread;
    private MenuItem mMenuItem;
    private SwipeRefreshLayout mRefreshLayout;
    private QMUITipDialog mDialog;

    @SuppressLint("RestrictedApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initToolbar();
        NavigationView navigationView = findViewById(R.id.navigation_drawer);

        mRefreshLayout = findViewById(R.id.refresh_layout);
        mRefreshLayout.setOnRefreshListener(this);

        mHandler = new Handler(getMainLooper()) {
            @Override
            public void handleMessage(Message msg) {
                mRefreshLayout.setRefreshing(false);
            }
        };

        navigationView.setNavigationItemSelectedListener(this);
        mMenuItem = navigationView.getMenu().findItem(R.id.shouye);

        mFragmentMap = new ArrayMap<>(1);
        mUpdateThread = new UpdateThread();
        mUpdateThread.start();
        showFragment(HomeFragment.class.getSimpleName());
    }

    private void showFragment(String fragmentTag) {
        mCurrentFragment = getFragmentByTag(fragmentTag);
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.fl_container, mCurrentFragment, fragmentTag)
                .commitNow();

        mUpdateThread.setBaseFragment(mCurrentFragment);

    }

    public void showLoadingDialog() {
        if (mDialog == null) {
            createLoadingDialog();
        }
        mDialog.show();
    }

    public void dismissLoadingDialog() {
        if (mDialog != null ) {
            Log.e(TAG, "dismissLoadingDialog: "  );
            mDialog.dismiss();
        }
    }

    private void createLoadingDialog() {
        QMUITipDialog.Builder builder = new QMUITipDialog.Builder(this);
        builder.setTipWord("获取数据中...");
        builder.setIconType(QMUITipDialog.Builder.ICON_TYPE_LOADING);
        mDialog = builder.create();
    }

    private BaseFragment getFragmentByTag(String fragmentTag) {
        BaseFragment baseFragment = mFragmentMap.get(fragmentTag);
        if (baseFragment != null) {
            return baseFragment;
        }
        switch (fragmentTag) {
            case Constants.FragmentTag.ALERTS_FRAGMENT:
                baseFragment = new AlertsFragment();
                break;
            case Constants.FragmentTag.INVASIONS_FRAGMENT:
                baseFragment = new InvasionsFragment();
                break;
            case Constants.FragmentTag.FISSURES_FRAGMENT:
                baseFragment = new FissuresFragment();
                break;
            case Constants.FragmentTag.SORTIE_FRAGMENT:
                baseFragment = new SortieFragment();
                break;
            case Constants.FragmentTag.HOME_FRAGMENT:
                baseFragment = new HomeFragment();
                break;
            default:
                baseFragment = new EmptyFragment();
                break;
        }
        mFragmentMap.put(fragmentTag, baseFragment);
        return baseFragment;
    }

    private void initToolbar() {
        mToolbar = findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        ActionBar supportActionBar = getSupportActionBar();
        if (supportActionBar != null) {
            supportActionBar.setDisplayHomeAsUpEnabled(true);
            supportActionBar.setHomeButtonEnabled(true);
        }
        mDrawerLayout = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(
                this, mDrawerLayout, mToolbar, R.string.app_name, R.string.app_name);
        actionBarDrawerToggle.syncState();
        mDrawerLayout.addDrawerListener(actionBarDrawerToggle);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        if (Objects.equals(mMenuItem, item)) {
            mDrawerLayout.closeDrawers();
            return false;
        } else {
            mMenuItem = item;
        }
        switch (item.getItemId()) {
            case R.id.shouye:
                showFragment(Constants.FragmentTag.HOME_FRAGMENT);
                mToolbar.setTitle("概况");
                break;
            case R.id.jingbao:
                showFragment(Constants.FragmentTag.ALERTS_FRAGMENT);
                mToolbar.setTitle("警报");
                break;
            case R.id.ruqin:
                showFragment(Constants.FragmentTag.INVASIONS_FRAGMENT);
                mToolbar.setTitle("入侵");
                break;
            case R.id.liefeng:
                showFragment(Constants.FragmentTag.FISSURES_FRAGMENT);
                mToolbar.setTitle("裂缝");
                break;
            case R.id.tuji:
                showFragment(Constants.FragmentTag.SORTIE_FRAGMENT);
                mToolbar.setTitle("突击");
                break;

            default:
                break;
        }
        mDrawerLayout.closeDrawers();
        return false;
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mUpdateThread != null) {
            mUpdateThread.stopThread();
        }
        mUpdateThread = null;
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mUpdateThread == null) {
            mUpdateThread = new UpdateThread(mCurrentFragment);
            mUpdateThread.start();
        }
    }

    @Override
    public void onRefresh() {
        if (mCurrentFragment != null) {
            mCurrentFragment.setUserVisibleHint(true);
        }
        mHandler.sendEmptyMessageDelayed(0, 1000);
    }
}
