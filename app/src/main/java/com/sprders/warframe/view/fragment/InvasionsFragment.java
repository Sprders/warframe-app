package com.sprders.warframe.view.fragment;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.sprders.warframe.Type;
import com.sprders.warframe.adapter.recyclerview.InvasionsAdapter;
import com.sprders.warframe.model.invasions.InvasionsModel;
import com.sprders.warframe.view.base.ListFragment;

public class InvasionsFragment extends ListFragment<InvasionsModel> {

    @Override
    public BaseQuickAdapter getAdapter() {
        return new InvasionsAdapter(mModels);
    }

    @Override
    public String getDataType() {
        return Type.INVASIONS;
    }

    @Override
    public Class<InvasionsModel> getModelClass() {
        return InvasionsModel.class;
    }

    @Override
    public boolean removeItemOrNot(InvasionsModel invasionsModel) {
        return invasionsModel.eta.trim().startsWith("-")
                || invasionsModel.completed
                || invasionsModel.completion < 0;
    }
}
