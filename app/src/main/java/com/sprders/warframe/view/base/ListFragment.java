package com.sprders.warframe.view.base;

import android.os.Bundle;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.alibaba.fastjson.JSON;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.sprders.warframe.Constants;
import com.sprders.warframe.Type;
import com.sprders.warframe.callback.AbstractRetrofit2Callback;
import com.sprders.warframe.net.NetService;
import com.sprders.warframe.utils.PerferencesUtils;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;

public abstract class ListFragment<T> extends BaseFragment {

    public BaseQuickAdapter mBaseQuickAdapter;

    protected List<T> mModels;



    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        int width = ViewGroup.LayoutParams.MATCH_PARENT;
        ViewGroup.LayoutParams params = new ViewGroup.LayoutParams(width, width);

        RecyclerView recyclerView = new RecyclerView(mMainActivity);
        recyclerView.setLayoutParams(params);
        return recyclerView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        RecyclerView recyclerView = (RecyclerView) view;

        recyclerView.setLayoutManager(new LinearLayoutManager(mMainActivity));
        mModels = new ArrayList<>(16);
        mBaseQuickAdapter = getAdapter();
        recyclerView.setAdapter(mBaseQuickAdapter);
        setUserVisibleHint(true);
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isAdded() && isVisibleToUser) {
            refreshView(true);
        }
    }

    @Override
    public void handleMessage(Message msg) {
        if (msg.what == COUNT_DOWN) {
            mBaseQuickAdapter.notifyDataSetChanged();
            startCountDown();
        }
    }

    private static final String TAG = "BaseFragment";

    private void refreshView(final boolean showLoading) {
        PerferencesUtils.updateUpdateTime();
        Log.e(TAG, "refreshView: ");
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.BASE_URL)
                .build();
        NetService service = retrofit.create(NetService.class);
        if (showLoading) {
            mHandler.sendEmptyMessage(SHOW_LOADING);
        }
        service.getDataFromServer(Constants.PLATFORM_PC, getDataType())
                .enqueue(new AbstractRetrofit2Callback<ResponseBody>() {

                    @Override
                    public void onSuccess(Call<ResponseBody> call, Response<ResponseBody> response) throws Exception {
                        ResponseBody body = response.body();
                        String result = "";
                        if (body != null) {
                            result = body.string();
                        }
                        refreshView(result);
                        mHandler.removeCallbacksAndMessages(null);
                        if (showLoading) {
                            mHandler.sendEmptyMessage(HIDE_LOADING);
                        }
                        startCountDown();
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        if (showLoading) {
                            mHandler.sendEmptyMessage(HIDE_LOADING);
                        }
                        Toast.makeText(mMainActivity, "网络异常！", Toast.LENGTH_SHORT).show();
                    }
                });
    }

    @Override
    public void refreshView() {
        refreshView(false);
    }

    protected void refreshView(String data) {
        List<T> list = JSON.parseArray(data, getModelClass());
        mModels.clear();
        if (list != null) {
            Iterator<T> iterator = list.iterator();
            while (iterator.hasNext()) {
                T next = iterator.next();
                if (removeItemOrNot(next)) {
                    iterator.remove();
                }
            }
            mModels.addAll(list);
        }
        mBaseQuickAdapter.notifyDataSetChanged();
    }

    protected void startCountDown() {
        mHandler.sendEmptyMessageDelayed(COUNT_DOWN, 1000);
    }


    public String getDataType() {
        String className = this.getClass().getSimpleName();
        switch (className) {
            case Constants.FragmentTag.ALERTS_FRAGMENT:
                return Type.ALERTS;
            case Constants.FragmentTag.FISSURES_FRAGMENT:
                return Type.FISSURES;
            case Constants.FragmentTag.INVASIONS_FRAGMENT:
                return Type.INVASIONS;
            default:
                return Object.class.getSimpleName();
        }
    }

    public boolean removeItemOrNot(T t) {
        return false;
    }

    public abstract BaseQuickAdapter getAdapter();

    public abstract Class<T> getModelClass();


}

