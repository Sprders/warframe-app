package com.sprders.warframe.view.fragment;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.sprders.warframe.Type;
import com.sprders.warframe.adapter.recyclerview.AlertsAdapter;
import com.sprders.warframe.model.alerts.AlertsModel;
import com.sprders.warframe.view.base.ListFragment;

public final class AlertsFragment extends ListFragment<AlertsModel> {

    @Override
    public BaseQuickAdapter getAdapter() {
        return new AlertsAdapter(mModels);
    }

    @Override
    public String getDataType() {
        return Type.ALERTS;
    }

    @Override
    public Class<AlertsModel> getModelClass() {
        return AlertsModel.class;
    }

    @Override
    public boolean removeItemOrNot(AlertsModel alertsModel) {
        return alertsModel.eta.trim().startsWith("-");
    }
}
