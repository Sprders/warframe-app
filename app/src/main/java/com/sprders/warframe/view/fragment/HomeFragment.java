package com.sprders.warframe.view.fragment;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.RelativeSizeSpan;
import android.text.style.StyleSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.alibaba.fastjson.JSON;
import com.qmuiteam.qmui.util.QMUIDisplayHelper;
import com.sprders.warframe.Constants;
import com.sprders.warframe.R;
import com.sprders.warframe.Type;
import com.sprders.warframe.callback.AbstractRetrofit2Callback;
import com.sprders.warframe.db.SortieHelper;
import com.sprders.warframe.model.dailydeals.DailyDealsModel;
import com.sprders.warframe.model.others.CetusModel;
import com.sprders.warframe.model.others.EarthModel;
import com.sprders.warframe.model.trader.VoidTraderModel;
import com.sprders.warframe.net.NetService;
import com.sprders.warframe.utils.ModelUtils;
import com.sprders.warframe.utils.PerferencesUtils;
import com.sprders.warframe.utils.TimeUtils;
import com.sprders.warframe.view.base.BaseFragment;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;

public final class HomeFragment extends BaseFragment {

    private long mCetusEta;
    private long mEarthEta;
    private long mTraderEta;
    private TextView mCetusTime;
    private TextView mEarthTime;
    private TextView mTradeTime;
    private TextView mDailyTime;
    private long mDailyEta;
    private TextView mTradeNode;
    private TextView mItemName;
    private TextView mItemDetail;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_home, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mCetusTime = view.findViewById(R.id.tv_cetus_time);
        mEarthTime = view.findViewById(R.id.tv_earth_time);
        mTradeTime = view.findViewById(R.id.tv_trade_time);
        mDailyTime = view.findViewById(R.id.tv_daily_time);
        mTradeNode = view.findViewById(R.id.tv_trade_node);
        mItemName = view.findViewById(R.id.tv_item_name);
        mItemDetail = view.findViewById(R.id.tv_item_detail);

        refreshView(true);
    }

    @Override
    public void handleMessage(Message msg) {
        if (msg.what == COUNT_DOWN) {
            mCetusEta -= 1;
            mEarthEta -= 1;
            mTraderEta -= 1;
            mDailyEta-=1;
            refreshCountDownView();
            startCountDown();
        }
    }

    public void refreshView(boolean showLoading) {
        PerferencesUtils.updateUpdateTime();
        NetService netService = new Retrofit.Builder().baseUrl(Constants.BASE_URL)
                .build()
                .create(NetService.class);
        if (showLoading) {
            mHandler.sendEmptyMessage(SHOW_LOADING);
        }
        String[] types = {Type.CETUS_CYCLE, Type.EARTH_CYCLE, Type.VOID_TRADER, Type.DAILY_DEALS};
        for (String type : types) {
            requestData(netService, type);
        }
    }


    @Override
    public void refreshView() {
        refreshView(false);
    }

    private void requestData(NetService netService, final String type) {
        netService.getDataFromServer("pc", type)
                .enqueue(new AbstractRetrofit2Callback<ResponseBody>() {
                    @Override
                    public void onSuccess(Call<ResponseBody> call, Response<ResponseBody> response) throws Exception {
                        String string = response.body().string();
                        switch (type) {
                            case Type.CETUS_CYCLE:
                                CetusModel cetusModel = JSON.parseObject(string, CetusModel.class);
                                mCetusEta = TimeUtils.parseTime2Long(cetusModel.expiry);
                                refreshCetusEarth(type, cetusModel.isDay);
                                break;
                            case Type.EARTH_CYCLE:
                                EarthModel earthModel = JSON.parseObject(string, EarthModel.class);
                                mEarthEta = TimeUtils.parseTime2Long(earthModel.expiry);
                                refreshCetusEarth(type, earthModel.isDay);
                                break;
                            case Type.VOID_TRADER:
                                VoidTraderModel voidTraderModel = JSON.parseObject(string, VoidTraderModel.class);
                                mTraderEta = TimeUtils.parseTime2Long(voidTraderModel.activation);
                                refreshDailyTrader(type, ModelUtils.translateNode2Zh(voidTraderModel.location), "");
                                break;
                            case Type.DAILY_DEALS:
                                List<DailyDealsModel> dailyDealsModels = JSON.parseArray(string, DailyDealsModel.class);
                                if (!dailyDealsModels.isEmpty()) {
                                    DailyDealsModel dailyDealsModel = dailyDealsModels.get(0);
                                    mDailyEta = TimeUtils.parseTime2Long(dailyDealsModel.expiry);
                                    StringBuilder builder = new StringBuilder("");
                                    builder.append("白金: ")
                                            .append(dailyDealsModel.salePrice)
                                            .append("\r\n折扣: ")
                                            .append(dailyDealsModel.discount)
                                            .append("%\r\n数量: ")
                                            .append(dailyDealsModel.total - dailyDealsModel.sold)
                                            .append("/")
                                            .append(dailyDealsModel.total);
                                    refreshDailyTrader(type, SortieHelper.getZh(dailyDealsModel.item), builder.toString());

                                }
                                break;
                            default:
                                break;
                        }
                        refreshCountDownView();
                        mHandler.removeCallbacksAndMessages(null);
                        mHandler.sendEmptyMessage(HIDE_LOADING);
                        startCountDown();
                    }
                });
    }

    private void refreshCetusEarth(String type, boolean isDay) {
        Drawable drawable;
        if (isDay) {
            drawable = mMainActivity.getDrawable(R.drawable.ic_day);
        } else {
            drawable = mMainActivity.getDrawable(R.drawable.ic_night);
        }
        drawable.setBounds(0, 0, QMUIDisplayHelper.dp2px(mMainActivity, 60),
                QMUIDisplayHelper.dp2px(mMainActivity, 60));
        switch (type) {
            case Type.CETUS_CYCLE:
                mCetusTime.setCompoundDrawablesRelative(drawable, null, null, null);
                break;
            case Type.EARTH_CYCLE:
                mEarthTime.setCompoundDrawablesRelative(drawable, null, null, null);
                break;
            default:
                break;
        }
    }

    private void refreshDailyTrader(String type, String content1, String content2) {
        switch (type) {
            case Type.VOID_TRADER:
                SpannableStringBuilder stringBuilder = new SpannableStringBuilder("奸商目的地\r\n");
                int length = stringBuilder.length();
                RelativeSizeSpan relativeSizeSpan = new RelativeSizeSpan(0.6F);
                stringBuilder.setSpan(relativeSizeSpan, 0, length, Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
                relativeSizeSpan = new RelativeSizeSpan(1.1F);
                stringBuilder.append(content1);
                stringBuilder.setSpan(relativeSizeSpan, length, stringBuilder.length(),
                        Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                stringBuilder.setSpan(new StyleSpan(android.graphics.Typeface.BOLD), length,
                        stringBuilder.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

                mTradeNode.setText(stringBuilder);
                break;
            case Type.DAILY_DEALS:
                mItemName.setText(content1);
                mItemDetail.setText(content2);
                break;
            default:
                break;
        }
    }

    private void startCountDown() {
        mHandler.sendEmptyMessageDelayed(COUNT_DOWN, 1000);
    }

    private void refreshCountDownView() {
        mCetusTime.setText(TimeUtils.parseLong2Time(mCetusEta));
        mEarthTime.setText(TimeUtils.parseLong2Time(mEarthEta));
        mDailyTime.setText(TimeUtils.parseLong2Time(mDailyEta));
        mTradeTime.setText(TimeUtils.parseLong2Time(mTraderEta));
    }
}
