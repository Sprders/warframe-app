package com.sprders.warframe.view.fragment;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.sprders.warframe.Type;
import com.sprders.warframe.adapter.recyclerview.FissuresAdapter;
import com.sprders.warframe.model.fissures.FissuresModel;
import com.sprders.warframe.view.base.ListFragment;

/**
 * 裂缝：https://api.warframestat.us/pc/fissures
 * @author sprders
 */
public final class FissuresFragment extends ListFragment<FissuresModel> {

    @Override
    public BaseQuickAdapter getAdapter() {
        return new FissuresAdapter(mModels);
    }

    @Override
    public String getDataType() {
        return Type.FISSURES;
    }

    @Override
    public Class<FissuresModel> getModelClass() {
        return FissuresModel.class;
    }

    @Override
    public boolean removeItemOrNot(FissuresModel fissuresModel) {
        return fissuresModel.expired || fissuresModel.eta.trim().startsWith("-");
    }
}
