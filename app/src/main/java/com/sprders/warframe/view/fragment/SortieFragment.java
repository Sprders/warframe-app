package com.sprders.warframe.view.fragment;

import android.os.Bundle;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.alibaba.fastjson.JSON;
import com.sprders.warframe.Constants;
import com.sprders.warframe.Type;
import com.sprders.warframe.R;
import com.sprders.warframe.adapter.recyclerview.SortieAdapter;
import com.sprders.warframe.callback.AbstractRetrofit2Callback;
import com.sprders.warframe.model.sortie.SortieModel;
import com.sprders.warframe.model.sortie.VariantsBean;
import com.sprders.warframe.net.NetService;
import com.sprders.warframe.utils.PerferencesUtils;
import com.sprders.warframe.utils.TimeUtils;
import com.sprders.warframe.view.base.BaseFragment;

import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;

public final class SortieFragment extends BaseFragment {


    private List<VariantsBean> mVariantsBeans;
    private SortieAdapter mSortieAdapter;
    private TextView mEndTime;
    private TextView mFaction;
    private TextView mFactionBoss;

    private long eta;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_sortie, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mEndTime = view.findViewById(R.id.tv_end_time);
        mFaction = view.findViewById(R.id.tv_faction);
        mFactionBoss = view.findViewById(R.id.tv_faction_boss);
        RecyclerView rv_sortie = view.findViewById(R.id.rv_sortie);

        mVariantsBeans = new ArrayList<>(6);
        rv_sortie.setLayoutManager(new LinearLayoutManager(mMainActivity));
        mSortieAdapter = new SortieAdapter(mVariantsBeans);
        rv_sortie.setAdapter(mSortieAdapter);
        refreshView(true);
    }

    private void refreshView(final boolean showLoading) {
        PerferencesUtils.updateUpdateTime();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.BASE_URL)
                .build();
        NetService service = retrofit.create(NetService.class);
        if (showLoading) {
            mHandler.sendEmptyMessage(SHOW_LOADING);
        }
        service.getDataFromServer(Constants.PLATFORM_PC, Type.SORTIE)
                .enqueue(new AbstractRetrofit2Callback<ResponseBody>() {

                    @Override
                    public void onSuccess(Call<ResponseBody> call, Response<ResponseBody> response) throws Exception {
                        ResponseBody body = response.body();
                        String result = "";
                        if (body != null) {
                            result = body.string();
                        }
                        SortieModel sortieModel = JSON.parseObject(result, SortieModel.class);
                        eta = TimeUtils.parseTime2Long(sortieModel.expiry);
                        mEndTime.setText(TimeUtils.parseLong2Time(eta));
                        mFaction.setText(sortieModel.faction);
                        mFactionBoss.setText(sortieModel.boss);
                        mVariantsBeans.clear();
                        mVariantsBeans.addAll(sortieModel.variants);
                        mHandler.removeCallbacksAndMessages(null);
                        if (showLoading) {
                            mHandler.sendEmptyMessage(HIDE_LOADING);
                        }
                        mSortieAdapter.notifyDataSetChanged();
                        startCountDown();
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        if (showLoading) {
                            mHandler.sendEmptyMessage(HIDE_LOADING);
                        }
                        Toast.makeText(mMainActivity, "网络异常！", Toast.LENGTH_SHORT).show();
                    }
                });
    }

    @Override
    public void refreshView() {
        refreshView(false);
    }

    public void startCountDown() {
        mHandler.sendEmptyMessageDelayed(COUNT_DOWN, 1000);
    }

    @Override
    public void handleMessage(Message msg) {
        if (msg.what == COUNT_DOWN) {
            eta -= 1;
            mEndTime.setText(TimeUtils.parseLong2Time(eta));
            mHandler.sendEmptyMessageDelayed(COUNT_DOWN, 1000);
        }
    }
}
