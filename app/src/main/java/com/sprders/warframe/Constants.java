package com.sprders.warframe;

public final class Constants {

    public static final String BASE_URL = "https://api.warframestat.us/";

    public static final String PLATFORM_PC = "pc";
    public static final String PLATFORM_XBOX1 = "xb1";
    public static final String PLATFORM_PS4 = "ps4";

    public static class FragmentTag {
        public static final String HOME_FRAGMENT = "HomeFragment";
        public static final String ALERTS_FRAGMENT = "AlertsFragment";
        public static final String INVASIONS_FRAGMENT = "InvasionsFragment";
        public static final String FISSURES_FRAGMENT = "FissuresFragment";
        public static final String SORTIE_FRAGMENT = "SortieFragment";
    }

}
