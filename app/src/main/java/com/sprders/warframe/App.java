package com.sprders.warframe;

import android.app.Application;
import android.content.Context;

import com.sprders.warframe.thread.CopyDbThread;

public final class App extends Application {

    private static Context mContext;
    private static final String TAG = "App";

    @Override
    public void onCreate() {
        super.onCreate();
        mContext = this;
        new CopyDbThread().start();
    }

    public static Context getContext() {
        return mContext;
    }
}
