package com.sprders.warframe.callback;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public abstract class AbstractRetrofit2Callback<T> implements Callback<T> {

    @Override
    public void onFailure(Call<T> call, Throwable t) {
    }

    @Override
    public void onResponse(Call<T> call, Response<T> response) {
        try {
            onSuccess(call, response);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public abstract void onSuccess(Call<T> call, Response<T> response) throws Exception;
}
