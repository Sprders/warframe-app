package com.sprders.warframe.callback;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.View;

public final class FragmentLifecycle extends FragmentManager.FragmentLifecycleCallbacks {
    private static final String TAG = "FragmentLifecycle";

    @Override
    public void onFragmentViewCreated(FragmentManager fm, Fragment f, View v, Bundle savedInstanceState) {
        Log.e(TAG, "onFragmentViewCreated: ");
        f.setUserVisibleHint(true);
        fm.unregisterFragmentLifecycleCallbacks(this);
    }
}
