package com.sprders.warframe.adapter.viewpagerad;

import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.sprders.warframe.view.base.BaseFragment;

import java.util.List;

public final class FragmentAdapter extends FragmentPagerAdapter {

    private List<BaseFragment> mFragmentList;
    private String[] mTitles;

    public void setFragmentList(List<BaseFragment> fragmentList) {
        mFragmentList = fragmentList;
    }

    public void setTitles(String[] titles) {
        mTitles = titles;
    }

    public FragmentAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        return mFragmentList.get(position);
    }

    @Override
    public int getCount() {
        return mFragmentList.size();
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return mTitles[position];
    }
}
