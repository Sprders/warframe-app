package com.sprders.warframe.adapter.recyclerview;

import android.support.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.sprders.warframe.R;
import com.sprders.warframe.db.SortieHelper;
import com.sprders.warframe.model.sortie.VariantsBean;
import com.sprders.warframe.utils.ModelUtils;
import com.sprders.warframe.utils.TaskUtil;

import java.util.List;

public final class SortieAdapter extends BaseQuickAdapter<VariantsBean, BaseViewHolder> {

    public SortieAdapter(@Nullable List<VariantsBean> data) {
        super(R.layout.item_sortie, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, VariantsBean item) {
        helper.setText(R.id.tv_mission_type, TaskUtil.coverEn2Zh(item.missionType))
                .setText(R.id.tv_node, ModelUtils.translateNode2Zh(item.node))
                .setText(R.id.tv_modifier, SortieHelper.getZh(item.modifier));
    }
}
