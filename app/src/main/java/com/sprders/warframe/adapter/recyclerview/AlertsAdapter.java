package com.sprders.warframe.adapter.recyclerview;

import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.ImageSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.StyleSpan;
import android.util.Log;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.sprders.warframe.App;
import com.sprders.warframe.R;
import com.sprders.warframe.model.alerts.AlertsModel;
import com.sprders.warframe.utils.ModelUtils;
import com.sprders.warframe.utils.TaskUtil;
import com.sprders.warframe.utils.TimeUtils;

import java.util.List;

/**
 * @author sprders
 */
public class AlertsAdapter extends BaseQuickAdapter<AlertsModel, BaseViewHolder> {

    public AlertsAdapter(@Nullable List<AlertsModel> data) {
        super(R.layout.item_alerts, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, AlertsModel item) {

        //节点
        SpannableStringBuilder stringBuilder = new SpannableStringBuilder("");
        stringBuilder.append(ModelUtils.translateNode2Zh(item.mission.node));
        stringBuilder.setSpan(new StyleSpan(android.graphics.Typeface.BOLD), 0, stringBuilder.length(),
                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        helper.setText(R.id.tv_node, stringBuilder);

        //剩余时间

        if (item.etaTime == 0) {
            if (!item.expired) {
                item.etaTime = TimeUtils.parseTime2Long(item.expiry);
            }
        } else {
            item.etaTime -= 1;
        }
        if (item.etaTime <= 0) {
            item.expired = true;
        }
        if (item.etaTime <= 0) {
            helper.setTextColor(R.id.tv_endtime, Color.GRAY);
        } else if (item.etaTime < 180) {
            helper.setTextColor(R.id.tv_endtime, Color.RED);
        } else {
            helper.setTextColor(R.id.tv_endtime, Color.BLACK);
        }
        helper.setText(R.id.tv_endtime, TimeUtils.parseLong2Time(item.etaTime));


        helper.setVisible(R.id.tv_level_range, item.mission.nightmare);

        //空战
        boolean archwingRequired = item.mission.archwingRequired;
        //类型
        String type = item.mission.type;
        stringBuilder = new SpannableStringBuilder("");
        stringBuilder.append(archwingRequired ? "A丨" : "");
        stringBuilder.append(TaskUtil.coverEn2Zh(type));
        int tempLength = stringBuilder.length();
        stringBuilder.setSpan(new RelativeSizeSpan(1.1f), 0, tempLength,
                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        stringBuilder.setSpan(new StyleSpan(android.graphics.Typeface.BOLD), 0, tempLength,
                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        stringBuilder.append("-");
        String faction = item.mission.faction;
        stringBuilder.append(faction);
        helper.setText(R.id.tv_type, stringBuilder);
        //奖励
        helper.setText(R.id.tv_reward, getReward(item));
    }


    private SpannableStringBuilder getReward(AlertsModel item) {
        SpannableStringBuilder stringBuilder = new SpannableStringBuilder();

        stringBuilder.append("奖励:");
        stringBuilder.append(String.valueOf(item.mission.reward.credits)).append("金钱");
        int tempLength = stringBuilder.length();

        stringBuilder.append(ModelUtils.translateItems2Zh(item.mission.reward.items));

        stringBuilder.append(ModelUtils.translateCountedItem2Zh(item.mission.reward.countedItems));

        Drawable drawable = App.getContext().getResources().getDrawable(R.drawable.ic_cr, null);
        drawable.setBounds(0, 0, 40, 40);
        ImageSpan imageSpan = new ImageSpan(drawable, ImageSpan.ALIGN_BASELINE);
        stringBuilder.setSpan(imageSpan, tempLength - 2, tempLength, Spanned.SPAN_INCLUSIVE_INCLUSIVE);
        return stringBuilder;
    }
}
