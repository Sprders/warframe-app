package com.sprders.warframe.adapter.recyclerview;

import android.support.annotation.Nullable;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.sprders.warframe.App;
import com.sprders.warframe.R;
import com.sprders.warframe.db.AlertsInvasionsHelper;
import com.sprders.warframe.model.common.CountedItemBean;
import com.sprders.warframe.model.invasions.InvasionsModel;
import com.sprders.warframe.utils.ModelUtils;

import java.text.DecimalFormat;
import java.util.List;

public final class InvasionsAdapter extends BaseQuickAdapter<InvasionsModel, BaseViewHolder> {
    public InvasionsAdapter(@Nullable List<InvasionsModel> data) {
        super(R.layout.item_invasions, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, InvasionsModel item) {
        helper.setText(R.id.tv_node, ModelUtils.translateNode2Zh(item.node))
                .setText(R.id.tv_defending_faction, item.defendingFaction)
                .setText(R.id.tv_attacking_faction, item.attackingFaction);

        StringBuilder builder = new StringBuilder("");
        for (CountedItemBean countedItem : item.defenderReward.countedItems) {
            if (countedItem.count <= 1) {
                builder.append(AlertsInvasionsHelper.getZh(countedItem.type));
            } else {
                builder.append(countedItem.count).append("*")
                        .append(AlertsInvasionsHelper.getZh(countedItem.type));
            }
        }
        helper.setText(R.id.tv_defender_reward,builder.toString());
        builder.delete(0,builder.length());

        for (CountedItemBean countedItem : item.attackerReward.countedItems) {
            if (countedItem.count <= 1) {
                builder.append(AlertsInvasionsHelper.getZh(countedItem.type));
            } else {
                builder.append(countedItem.count).append("*")
                        .append(AlertsInvasionsHelper.getZh(countedItem.type));
            }
        }
        helper.setText(R.id.tv_attacker_reward,builder.toString());



        double completion = item.completion;
        TextView textView = helper.getView(R.id.tv_completion_line);
        int width = (int) (dp2px(300) * completion / 100);
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(width, dp2px(10));
        textView.setLayoutParams(params);

        helper.setText(R.id.tv_completion_num,
                new DecimalFormat("#0.00").format(item.completion) + "%");

    }

    private int dp2px(float dpValue) {
        float scale = App.getContext().getResources().getDisplayMetrics().density;
        return (int) (dpValue * scale + 0.5f);
    }
}
