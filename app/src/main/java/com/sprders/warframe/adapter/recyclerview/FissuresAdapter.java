package com.sprders.warframe.adapter.recyclerview;

import android.graphics.Color;
import android.support.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.sprders.warframe.R;
import com.sprders.warframe.model.fissures.FissuresModel;
import com.sprders.warframe.utils.ModelUtils;
import com.sprders.warframe.utils.TaskUtil;
import com.sprders.warframe.utils.TimeUtils;

import java.util.List;

public final class FissuresAdapter extends BaseQuickAdapter<FissuresModel, BaseViewHolder> {
    public FissuresAdapter(@Nullable List<FissuresModel> data) {
        super(R.layout.item_fissures, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, FissuresModel item) {

        //剩余时间
        if (item.etaTime == 0) {
            if (!item.expired) {
                item.etaTime = TimeUtils.parseTime2Long(item.expiry);
            }
        } else {
            item.etaTime -= 1;
        }
        if (item.etaTime <= 0) {
            item.expired = true;
            helper.setTextColor(R.id.tv_endtime, Color.GRAY);
        } else if (item.etaTime < 180) {
            helper.setTextColor(R.id.tv_endtime, Color.RED);
        } else {
            helper.setTextColor(R.id.tv_endtime, Color.BLACK);
        }
        helper.setText(R.id.tv_endtime, TimeUtils.parseLong2Time(item.etaTime));


        helper.setText(R.id.tv_node, ModelUtils.translateNode2Zh(item.node))
                .setText(R.id.tv_type, TaskUtil.coverEn2Zh(item.missionType) + "-" + item.enemy)
                .setText(R.id.tv_tier, getTierByNum(item.tierNum))
                .setText(R.id.tv_endtime, TimeUtils.parseLong2Time(item.etaTime));
    }

    public String getTierByNum(int tiernum) {
        switch (tiernum) {
            case 1:
                return "古纪";
            case 2:
                return "前纪";
            case 3:
                return "中纪";
            case 4:
                return "后纪";
            default:
                return "未知纪元";
        }
    }
}
