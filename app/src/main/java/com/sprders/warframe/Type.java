package com.sprders.warframe;

public class Type {
    public static final String ALERTS = "alerts";
    public static final String INVASIONS = "invasions";
    public static final String NEWS = "news";
    public static final String FISSURES = "fissures";
    public static final String SORTIE = "sortie";
    public static final String SYNDICATE_MISSIONS = "syndicateMissions";

    public static final String CETUS_CYCLE = "cetusCycle";
    public static final String EARTH_CYCLE = "earthCycle";
    public static final String VOID_TRADER = "voidTrader";
    public static final String DAILY_DEALS = "dailyDeals";
}