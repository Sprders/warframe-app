package com.sprders.warframe.model.syndicateMissions;

import com.alibaba.fastjson.annotation.JSONField;

import java.util.List;

public class Job {

    @JSONField(name = "id")
    public String id;
    @JSONField(name = "type")
    public String type;
    @JSONField(name = "enemyLevels")
    public List<Integer> enemyLevels ;
    @JSONField(name = "standingStages")
    public List<Integer> standingStages ;
    @JSONField(name = "rewardPool")
    public List<String> rewardPool ;
}