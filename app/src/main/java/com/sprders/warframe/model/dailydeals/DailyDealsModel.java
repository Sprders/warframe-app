package com.sprders.warframe.model.dailydeals;

import com.alibaba.fastjson.annotation.JSONField;

public final class DailyDealsModel {


    @JSONField(name = "item")
    public String item;
    @JSONField(name = "expiry")
    public String expiry;
    @JSONField(name = "originalPrice")
    public int originalPrice;
    @JSONField(name = "salePrice")
    public int salePrice;
    @JSONField(name = "total")
    public int total;
    @JSONField(name = "sold")
    public int sold;
    @JSONField(name = "id")
    public String id;
    @JSONField(name = "eta")
    public String eta;
    @JSONField(name = "discount")
    public int discount;
}
