package com.sprders.warframe.model.alerts;

import com.alibaba.fastjson.annotation.JSONField;
import com.sprders.warframe.model.common.RewardBean;

public class MissionBean  {
    /**
     * node : Metis (Jupiter)
     * type : Rescue
     * faction : Corpus
     * reward : {"items":[],"countedItems":[],"credits":7400,"asString":"7400cr","itemString":"","thumbnail":"https://i.imgur.com/JCKyUXJ.png","color":15844367}
     * minEnemyLevel : 18
     * maxEnemyLevel : 20
     * nightmare : false
     * archwingRequired : false
     */

    @JSONField(name = "node")
    public String node;
    @JSONField(name = "type")
    public String type;
    @JSONField(name = "faction")
    public String faction;
    @JSONField(name = "reward")
    public RewardBean reward;
    @JSONField(name = "minEnemyLevel")
    public int minEnemyLevel;
    @JSONField(name = "maxEnemyLevel")
    public int maxEnemyLevel;
    @JSONField(name = "nightmare")
    public boolean nightmare;
    @JSONField(name = "archwingRequired")
    public boolean archwingRequired;

}