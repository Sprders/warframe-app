package com.sprders.warframe.model.sortie;

import com.alibaba.fastjson.annotation.JSONField;

import java.util.List;

public final class SortieModel {

    /**
     * id : 5b2bcb82832beb10b1bb445e
     * activation : 2018-06-21T16:00:02.016Z
     * expiry : 2018-06-22T15:59:00.000Z
     * rewardPool : Sortie Rewards
     * variants : [{"boss":"Deprecated","planet":"Deprecated","missionType":"Hive","modifier":"Augmented Enemy Armor","modifierDescription":"Enemies have Improved/Added armor. Corrosive Projection effects are halved.","node":"Brugia (Eris)"},{"boss":"Deprecated","planet":"Deprecated","missionType":"Defense","modifier":"Weapon Restriction: Shotgun Only","modifierDescription":"Only shotguns may be used in this mission, any other weapon type is not allowed, and will be removed automatically if equipped.","node":"Callisto (Jupiter)"},{"boss":"Deprecated","planet":"Deprecated","missionType":"Spy","modifier":"Environmental Hazard: Fire","modifierDescription":"The tileset has a fire hazard. Warframe health is halved. Meltdown Iminent.","node":"Roche (Phobos)"}]
     * boss : Mutalist Alad V
     * faction : Infestation
     * expired : false
     * eta : 8h 28m 32s
     */

    @JSONField(name = "id")
    public String id;
    @JSONField(name = "activation")
    public String activation;
    @JSONField(name = "expiry")
    public String expiry;
    @JSONField(name = "rewardPool")
    public String rewardPool;
    @JSONField(name = "boss")
    public String boss;
    @JSONField(name = "faction")
    public String faction;
    @JSONField(name = "expired")
    public boolean expired;
    @JSONField(name = "eta")
    public String eta;
    @JSONField(name = "variants")
    public List<VariantsBean> variants;
    @JSONField(serialize = false)
    public long etaTime;

}
