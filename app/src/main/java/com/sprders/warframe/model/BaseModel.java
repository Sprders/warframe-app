package com.sprders.warframe.model;

import com.alibaba.fastjson.JSON;

public abstract class BaseModel {
    @Override
    public String toString() {
        return JSON.toJSONString(this);
    }
}
