package com.sprders.warframe.model.syndicateMissions;

import com.alibaba.fastjson.annotation.JSONField;

import java.util.List;

public final class SyndicateMissionsModel {

    /**
     * id : 1529683140000Arbiters of Hexis
     * activation : 2018-06-21T15:59:01.537Z
     * expiry : 2018-06-22T15:59:00.000Z
     * syndicate : Arbiters of Hexis
     * nodes : ["Kiliken (Venus)","Terminus (Mercury)","Sharpless (Phobos)","Ananke (Jupiter)","Proteus (Neptune)","Hydron (Sedna)","Arval (Mars)"]
     * jobs : []
     * eta : 8h 12m 32s
     */

    @JSONField(name = "id")
    public String id;
    @JSONField(name = "activation")
    public String activation;
    @JSONField(name = "expiry")
    public String expiry;
    @JSONField(name = "syndicate")
    public String syndicate;
    @JSONField(name = "eta")
    public String eta;
    @JSONField(name = "nodes")
    public List<String> nodes;
    @JSONField(name = "jobs")
    public List<Job> jobs;
}
