package com.sprders.warframe.model.sortie;

import com.alibaba.fastjson.annotation.JSONField;

public class VariantsBean {
        /**
         * boss : Deprecated
         * planet : Deprecated
         * missionType : Hive
         * modifier : Augmented Enemy Armor
         * modifierDescription : Enemies have Improved/Added armor. Corrosive Projection effects are halved.
         * node : Brugia (Eris)
         */

        @JSONField(name = "boss")
        public String boss;
        @JSONField(name = "planet")
        public String planet;
        @JSONField(name = "missionType")
        public String missionType;
        @JSONField(name = "modifier")
        public String modifier;
        @JSONField(name = "modifierDescription")
        public String modifierDescription;
        @JSONField(name = "node")
        public String node;
    }