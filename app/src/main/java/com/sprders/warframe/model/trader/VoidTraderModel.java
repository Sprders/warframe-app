package com.sprders.warframe.model.trader;

import com.alibaba.fastjson.annotation.JSONField;

import java.util.List;

public final class VoidTraderModel {

    @JSONField(name = "id")
    public String id;
    @JSONField(name = "activation")
    public String activation;
    @JSONField(name = "expiry")
    public String expiry;
    @JSONField(name = "character")
    public String character;
    @JSONField(name = "location")
    public String location;
    @JSONField(name = "psId")
    public String psId;
    @JSONField(name = "active")
    public boolean active;
    @JSONField(name = "startString")
    public String startString;
    @JSONField(name = "endString")
    public String endString;
    @JSONField(name = "inventory")
    public List<InventoryBean> inventory;
}
