package com.sprders.warframe.model.others;

import com.alibaba.fastjson.annotation.JSONField;

public class CetusModel {

    @JSONField(name = "id")
    public String id;
    @JSONField(name = "expiry")
    public String expiry;
    @JSONField(name = "isDay")
    public boolean isDay;
    @JSONField(name = "timeLeft")
    public String timeLeft;
    @JSONField(name = "isCetus")
    public boolean isCetus;
    @JSONField(name = "shortString")
    public String shortString;
    @JSONField(serialize = false)
    public long etaTime;
}
