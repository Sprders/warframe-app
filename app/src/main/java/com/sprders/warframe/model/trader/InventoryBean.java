package com.sprders.warframe.model.trader;

import com.alibaba.fastjson.annotation.JSONField;

class InventoryBean {
    @JSONField(name = "item")
    public String item;
    @JSONField(name = "ducats")
    public int ducats;
    @JSONField(name = "credits")
    public int credits;
}
