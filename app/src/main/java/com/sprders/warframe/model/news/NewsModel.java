package com.sprders.warframe.model.news;

import com.alibaba.fastjson.annotation.JSONField;

public final class NewsModel {

    /**
     * id : 5ac3c6732183db65ee4c0856
     * message : TennoCon Digital Pack Available Now!
     * link : https://www.warframe.com/news/tennocon-2018-digital-pack-available-now
     * imageLink : https://forums.warframe.com/applications/core/interface/imageproxy/imageproxy.php?img=https://n9e5v4d8.ssl.hwcdn.net/uploads/ae293353b6531bfa27b647d89661068c.jpg&key=1b82a3a8170ec9aaa1eabd0d4eb0add490fe1e22236b5c68605fc0dea067ceba
     * priority : false
     * date : 2018-04-03T18:21:27.000Z
     * eta : 79d 13h 5m 59s ago
     * update : false
     * primeAccess : false
     * stream : false
     * translations : {"en":"TennoCon Digital Pack Available Now!","fr":"Pack Digital pour TennoCon Maintenant Dispo!","it":"Pacchetto TennoCon Digitale Ora Disponibile!","de":"Digitales TennoCon-Paket jetzt verfügbar!","es":"¡El Paquete Digital de TennoCon ya está disponible!","pt":"Pacote Digital da TennoCon já disponível!","ru":"Доступен цифровой пакет ТэнноКон!","tr":"TennoCon Dijital Paketi Sizlerle!","ja":"TennoCon デジタルパックが登場！","zh":"TennoCon 虚拟礼包现正登场！","ko":"텐노콘 디지털 팩 출시!","tc":"TennoCon 數位包現正登場！"}
     * asString : [79d 13h 5m 59s ago] [TennoCon Digital Pack Available Now!](https://www.warframe.com/news/tennocon-2018-digital-pack-available-now)
     */

    @JSONField(name = "id")
    public String id;
    @JSONField(name = "message")
    public String message;
    @JSONField(name = "link")
    public String link;
    @JSONField(name = "imageLink")
    public String imageLink;
    @JSONField(name = "priority")
    public boolean priority;
    @JSONField(name = "date")
    public String date;
    @JSONField(name = "eta")
    public String eta;
    @JSONField(name = "update")
    public boolean update;
    @JSONField(name = "primeAccess")
    public boolean primeAccess;
    @JSONField(name = "stream")
    public boolean stream;
    @JSONField(name = "translations")
    public TranslationsBean translations;
    @JSONField(name = "asString")
    public String asString;


}
