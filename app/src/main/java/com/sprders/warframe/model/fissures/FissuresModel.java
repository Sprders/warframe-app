package com.sprders.warframe.model.fissures;

import com.alibaba.fastjson.annotation.JSONField;

public final class FissuresModel {

    /**
     * id : 5b2c99850e054f39ee86ff2d
     * node : Despina (Neptune)
     * missionType : Excavation
     * enemy : Corpus
     * tier : Neo
     * tierNum : 3
     * activation : 2018-06-22T06:39:01.463Z
     * expiry : 2018-06-22T08:09:35.848Z
     * expired : false
     * eta : 41m 8s
     */

    @JSONField(name = "id")
    public String id;
    @JSONField(name = "node")
    public String node;
    @JSONField(name = "missionType")
    public String missionType;
    @JSONField(name = "enemy")
    public String enemy;
    @JSONField(name = "tier")
    public String tier;
    @JSONField(name = "tierNum")
    public int tierNum;
    @JSONField(name = "activation")
    public String activation;
    @JSONField(name = "expiry")
    public String expiry;
    @JSONField(name = "expired")
    public boolean expired;
    @JSONField(name = "eta")
    public String eta;
    @JSONField(serialize = false)
    public long etaTime;
}
