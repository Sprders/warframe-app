package com.sprders.warframe.model.common;

import com.alibaba.fastjson.annotation.JSONField;
import com.sprders.warframe.model.common.CountedItemBean;

import java.util.List;

public class RewardBean  {
    /**
     * items : []
     * countedItems : []
     * credits : 7400
     * asString : 7400cr
     * itemString :
     * thumbnail : https://i.imgur.com/JCKyUXJ.png
     * color : 15844367
     */

    @JSONField(name = "credits")
    public int credits;
    @JSONField(name = "asString")
    public String asString;
    @JSONField(name = "itemString")
    public String itemString;
    @JSONField(name = "thumbnail")
    public String thumbnail;
    @JSONField(name = "color")
    public int color;
    @JSONField(name = "items")
    public List<String> items;
    @JSONField(name = "countedItems")
    public List<CountedItemBean> countedItems;

}