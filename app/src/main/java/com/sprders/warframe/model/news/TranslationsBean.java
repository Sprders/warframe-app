package com.sprders.warframe.model.news;

import com.alibaba.fastjson.annotation.JSONField;

public  class TranslationsBean {
        /**
         * en : TennoCon Digital Pack Available Now!
         * fr : Pack Digital pour TennoCon Maintenant Dispo!
         * it : Pacchetto TennoCon Digitale Ora Disponibile!
         * de : Digitales TennoCon-Paket jetzt verfügbar!
         * es : ¡El Paquete Digital de TennoCon ya está disponible!
         * pt : Pacote Digital da TennoCon já disponível!
         * ru : Доступен цифровой пакет ТэнноКон!
         * tr : TennoCon Dijital Paketi Sizlerle!
         * ja : TennoCon デジタルパックが登場！
         * zh : TennoCon 虚拟礼包现正登场！
         * ko : 텐노콘 디지털 팩 출시!
         * tc : TennoCon 數位包現正登場！
         */

        @JSONField(name = "en")
        public String en;
        @JSONField(name = "fr")
        public String fr;
        @JSONField(name = "it")
        public String it;
        @JSONField(name = "de")
        public String de;
        @JSONField(name = "es")
        public String es;
        @JSONField(name = "pt")
        public String pt;
        @JSONField(name = "ru")
        public String ru;
        @JSONField(name = "tr")
        public String tr;
        @JSONField(name = "ja")
        public String ja;
        @JSONField(name = "zh")
        public String zh;
        @JSONField(name = "ko")
        public String ko;
        @JSONField(name = "tc")
        public String tc;
    }