package com.sprders.warframe.model.others;

import com.alibaba.fastjson.annotation.JSONField;

public final class EarthModel {

    /**
     * id : earthCyle1530259200004
     * expiry : 2018-06-29T08:00:00.004Z
     * isDay : false
     * timeLeft : 59m 37s
     */

    @JSONField(name = "id")
    public String id;
    @JSONField(name = "expiry")
    public String expiry;
    @JSONField(name = "isDay")
    public boolean isDay;
    @JSONField(name = "timeLeft")
    public String timeLeft;
    @JSONField(serialize = false)
    public long etaTime;
}
