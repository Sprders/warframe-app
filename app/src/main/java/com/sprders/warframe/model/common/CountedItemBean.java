package com.sprders.warframe.model.common;

import com.alibaba.fastjson.annotation.JSONField;

public class CountedItemBean {

    @JSONField(name = "count")
  public   int count;
    @JSONField(name = "type")
   public String type;

}
