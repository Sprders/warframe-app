package com.sprders.warframe.model.alerts;

import com.alibaba.fastjson.annotation.JSONField;
import com.sprders.warframe.model.BaseModel;

import java.util.List;
import java.util.Objects;

public class AlertsModel {

    /**
     * id : 5b2c44adea145ae3fd780345
     * activation : 2018-06-22T00:41:19.661Z
     * expiry : 2018-06-22T01:33:51.806Z
     * mission : {"node":"Metis (Jupiter)","type":"Rescue","faction":"Corpus","reward":{"items":[],"countedItems":[],"credits":7400,"asString":"7400cr","itemString":"","thumbnail":"https://i.imgur.com/JCKyUXJ.png","color":15844367},"minEnemyLevel":18,"maxEnemyLevel":20,"nightmare":false,"archwingRequired":false}
     * expired : false
     * eta : 14s
     * rewardTypes : ["credits"]
     */

    @JSONField(name = "id")
    public String id;
    @JSONField(name = "activation")
    public String activation;
    @JSONField(name = "expiry")
    public String expiry;
    @JSONField(name = "mission")
    public MissionBean mission;
    @JSONField(name = "expired")
    public boolean expired;
    @JSONField(name = "eta")
    public String eta;
    @JSONField(name = "rewardTypes")
    public List<String> rewardTypes;
    @JSONField(serialize = false)
    public long etaTime;

    @Override
    public boolean equals(Object o) {
        if (this == o){ return true;}
        if (o == null || getClass() != o.getClass()) {return false;}
        AlertsModel that = (AlertsModel) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
