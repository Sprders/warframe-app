package com.sprders.warframe.model.invasions;

import com.alibaba.fastjson.annotation.JSONField;
import com.sprders.warframe.model.common.RewardBean;

import java.util.List;

public final class InvasionsModel {


    /**
     * id : 5b284b05d3d6c18825bd32e6
     * node : Cerberus (Pluto)
     * desc : Grineer Offensive
     * attackerReward : {"items":[],"countedItems":[{"count":3,"type":"Detonite Injector"}],"credits":0,"asString":"3 Detonite Injector","itemString":"3 Detonite Injector","thumbnail":"https://i.imgur.com/rV6lN4W.png","color":5068118}
     * attackingFaction : Grineer
     * defenderReward : {"items":[],"countedItems":[{"count":3,"type":"Fieldron"}],"credits":0,"asString":"3 Fieldron","itemString":"3 Fieldron","thumbnail":"https://i.imgur.com/qlrlfft.png","color":5068118}
     * defendingFaction : Corpus
     * vsInfestation : false
     * activation : 2018-06-19T23:29:59.724Z
     * count : 22126
     * requiredRuns : 41000
     * completion : 76.9829268292683
     * completed : false
     * eta : 1d 23h 32m 53s
     * rewardTypes : ["detonite","fieldron"]
     */

    @JSONField(name = "id")
    public String id;
    @JSONField(name = "node")
    public String node;
    @JSONField(name = "desc")
    public String desc;
    @JSONField(name = "attackerReward")
    public RewardBean attackerReward;
    @JSONField(name = "attackingFaction")
    public String attackingFaction;
    @JSONField(name = "defenderReward")
    public RewardBean defenderReward;
    @JSONField(name = "defendingFaction")
    public String defendingFaction;
    @JSONField(name = "vsInfestation")
    public boolean vsInfestation;
    @JSONField(name = "activation")
    public String activation;
    @JSONField(name = "count")
    public int count;
    @JSONField(name = "requiredRuns")
    public int requiredRuns;
    @JSONField(name = "completion")
    public double completion;
    @JSONField(name = "completed")
    public boolean completed;
    @JSONField(name = "eta")
    public String eta;
    @JSONField(name = "rewardTypes")
    public List<String> rewardTypes;

}
